# [Android] MyGameCollection

MyGameCollection is an Android app developed with Unity that allows the user to register its videogame collection,
storing basic data of each Game, such as its name, its platform, the banner and more.

The main feature of this app, and the reason it was created, is that it allows to store the real played time, starting the clock 
when the gaming session starts and stopping it and its end.
